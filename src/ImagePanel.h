#pragma once
#include <wx/image.h>
#include <wx/panel.h>

#include <memory>
#include <string>
class Effect;

class ImagePanel : public wxPanel
{
 public:
  ImagePanel(wxFrame* parent, wxString imageFileName, wxBitmapType format);
  void applyEffect(std::shared_ptr<Effect>& effect);
  void paintEvent(wxPaintEvent& evt);
  void keyPressed(wxKeyEvent& evt);
  void saveImage(const wxString& fileName, wxBitmapType format) const;
  void mouseWheelMoved(wxMouseEvent& event);

  DECLARE_EVENT_TABLE()
 private:
  void render(wxDC& dc);
  wxImage m_image;
  std::shared_ptr<Effect> m_currentEffect;
};
