#include "QuantizeEffect.h"

#include "wx/rawbmp.h"

QuantizeEffect::QuantizeEffect(unsigned char numberOfColors)
    : Effect("Quantize", "perform color quantization"), m_numberOfcolors(numberOfColors)
{
}

QuantizeEffect::~QuantizeEffect() {}

void QuantizeEffect::apply(wxImage& image) const
{
  wxImagePixelData::Iterator pixelIterator(image);
  for (int row = 0; row < image.GetHeight(); row++)
  {
    wxImagePixelData::Iterator rowStart = pixelIterator;
    for (int column = 0; column < image.GetWidth(); column++)
    {
      quantizePixelValue(pixelIterator.Red());
      quantizePixelValue(pixelIterator.Green());
      quantizePixelValue(pixelIterator.Blue());
      pixelIterator++;
    }
    pixelIterator = rowStart;
    pixelIterator.OffsetY(image, 1);
  }
}

void QuantizeEffect::changeYParameter(int deltaX)
{
  if ((int)m_numberOfcolors + deltaX < 2)
  {
    m_numberOfcolors = 2;
  }
  else if ((int)m_numberOfcolors + deltaX > 255)
  {
    m_numberOfcolors = 255;
  }
  else
  {
    m_numberOfcolors += deltaX;
  }
}
void QuantizeEffect::changeXParameter(int deltaY) {}

void QuantizeEffect::quantizePixelValue(unsigned char& value) const
{
  value = roundPixelValue(((double)value * (double)m_numberOfcolors) / 255.0) * (255 / m_numberOfcolors);
}
