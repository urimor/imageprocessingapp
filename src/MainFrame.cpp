#include "MainFrame.h"

#include <wx/wx.h>

#include "Effect.h"
#include "ImagePanel.h"

MainFrame::MainFrame() : wxFrame(NULL, wxID_ANY, "Image Processing App")
{
  wxMenu *menuFile = new wxMenu;
  menuFile->Append(wxID_OPEN);
  menuFile->Append(wxID_SAVEAS);
  menuFile->AppendSeparator();
  menuFile->Append(wxID_EXIT);
  wxMenu *menuHelp = new wxMenu;
  menuHelp->Append(wxID_ABOUT);
  m_effectsMenu = new wxMenu;
  wxMenuBar *menuBar = new wxMenuBar;
  menuBar->Append(menuFile, "&File");
  menuBar->Append(menuHelp, "&Help");
  menuBar->Append(m_effectsMenu, "&Effects");
  SetMenuBar(menuBar);
  CreateStatusBar();
  SetStatusText("Welcome to the image processing app!");
  Bind(wxEVT_MENU, &MainFrame::OnOpen, this, wxID_OPEN);
  Bind(wxEVT_MENU, &MainFrame::OnSaveAs, this, wxID_SAVEAS);
  Bind(wxEVT_MENU, &MainFrame::OnAbout, this, wxID_ABOUT);
  Bind(wxEVT_MENU, &MainFrame::OnExit, this, wxID_EXIT);
}
void MainFrame::AddEffect(std::shared_ptr<Effect> effect)
{
  AddEffect(effect, effect->getName(), effect->getDescription());
}
void MainFrame::AddEffect(std::shared_ptr<Effect> effect, const std::string &text, const std::string &help)
{
  wxMenuItem *menuItem = m_effectsMenu->Append(wxID_ANY, text, help);
  Bind(
      wxEVT_MENU, [this, effect](wxCommandEvent &event) { OnEffectApplied(effect); }, menuItem->GetId());
}

void MainFrame::OnExit(wxCommandEvent &event)
{
  Close(true);
}
void MainFrame::OnAbout(wxCommandEvent &event)
{
  wxMessageBox("By Uri Mor", "Image Processing App", wxOK | wxICON_INFORMATION);
}
void MainFrame::OnEffectApplied(std::shared_ptr<Effect> effect)
{
  if (m_imagePanel != nullptr)
  {
    m_imagePanel->applyEffect(effect);
  }
}

void MainFrame::OnOpen(wxCommandEvent &event)
{
  wxFileDialog openFileDialog(this, _("Open an image"), "", "", "PNG files (*.png)|*.png",
                              wxFD_OPEN | wxFD_FILE_MUST_EXIST);
  if (openFileDialog.ShowModal() == wxID_CANCEL)
  {
    return;
  }
  if (m_imagePanel != nullptr)
  {
    m_imagePanel->Destroy();
    SetSizer(nullptr);
  }
  wxBoxSizer *sizer = new wxBoxSizer(wxHORIZONTAL);

  SetSizer(sizer);
  m_imagePanel = new ImagePanel(this, openFileDialog.GetPath(), wxBITMAP_TYPE_PNG);
  sizer->Add(m_imagePanel, 1, wxEXPAND);
  m_imagePanel->SetFocus();
  Fit();
}

void MainFrame::OnSaveAs(wxCommandEvent &event)
{
  if (m_imagePanel)
  {
    wxFileDialog saveFileDialog(this, _("Save Image As"), "", "", "PNG files (*.png)|*.png",
                                wxFD_SAVE | wxFD_OVERWRITE_PROMPT);
    if (saveFileDialog.ShowModal() == wxID_CANCEL)
    {
      return;
    }
    m_imagePanel->saveImage(saveFileDialog.GetPath(), wxBITMAP_TYPE_PNG);
  }
}
