#include "Effect.h"

Effect::Effect(std::string name, std::string description) : m_name(name), m_description(description) {}

Effect::~Effect() {}

const std::string& Effect::getName() const
{
  return m_name;
}

const std::string& Effect::getDescription() const
{
  return m_description;
}

unsigned char Effect::roundPixelValue(double value)
{
  double roundedValue = round(value);
  if (roundedValue < 0)
  {
    return 0;
  }
  else if (roundedValue > 255)
  {
    return (unsigned char)255;
  }
  else
  {
    return (unsigned char)roundedValue;
  }
}
