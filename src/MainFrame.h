#pragma once
#include <wx/frame.h>

#include <memory>
class ImagePanel;
class Effect;

class MainFrame : public wxFrame
{
 public:
  MainFrame();
  void AddEffect(std::shared_ptr<Effect> effect);
  void AddEffect(std::shared_ptr<Effect> effect, const std::string &text, const std::string &help);

 private:
  void OnOpen(wxCommandEvent &event);
  void OnSaveAs(wxCommandEvent &event);
  void OnExit(wxCommandEvent &event);
  void OnAbout(wxCommandEvent &event);
  void OnEffectApplied(std::shared_ptr<Effect>);

  ImagePanel *m_imagePanel;
  wxMenu *m_effectsMenu;
};
