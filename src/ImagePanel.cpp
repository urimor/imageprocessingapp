#include "ImagePanel.h"

#include "Effect.h"
#include "wx/wx.h"

using namespace std;

BEGIN_EVENT_TABLE(ImagePanel, wxPanel)

// catch paint events
EVT_PAINT(ImagePanel::paintEvent)
EVT_KEY_DOWN(ImagePanel::keyPressed)
EVT_MOUSEWHEEL(ImagePanel::mouseWheelMoved)

END_EVENT_TABLE()

ImagePanel::ImagePanel(wxFrame* parent, wxString imageFileName, wxBitmapType format)
    : wxPanel(parent, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxWANTS_CHARS)

{
  m_image.LoadFile(imageFileName, format);
  wxClientDC dc(this);
  render(dc);
  this->SetMinSize(m_image.GetSize());
}

void ImagePanel::applyEffect(std::shared_ptr<Effect>& effect)
{
  m_currentEffect = effect;
  wxClientDC dc(this);
  render(dc);
}

void ImagePanel::paintEvent(wxPaintEvent& evt)
{
  wxPaintDC dc(this);
  render(dc);
}

void ImagePanel::keyPressed(wxKeyEvent& evt)
{
  if (m_currentEffect == nullptr)
  {
    return;
  }
  wxClientDC dc(this);
  switch (evt.GetKeyCode())
  {
    case WXK_RIGHT:
      m_currentEffect->changeXParameter(1);
      render(dc);
      break;
    case WXK_LEFT:
      m_currentEffect->changeXParameter(-1);
      render(dc);
      break;
    case WXK_UP:
      m_currentEffect->changeYParameter(1);
      render(dc);
      break;
    case WXK_DOWN:
      m_currentEffect->changeYParameter(-1);
      render(dc);
      break;
    default:
      break;
  }
}

void ImagePanel::saveImage(const wxString& fileName, wxBitmapType format) const
{
  if (m_currentEffect != nullptr)
  {
    wxImage imageAfterEffect = m_image.Copy();
    m_currentEffect->apply(imageAfterEffect);
    imageAfterEffect.SaveFile(fileName, format);
  }
  else
  {
    m_image.SaveFile(fileName, format);
  }
}

void ImagePanel::mouseWheelMoved(wxMouseEvent& event)
{
  if (m_currentEffect != nullptr)
  {
    int step = event.GetWheelRotation() / event.GetWheelDelta();
    if (event.GetWheelAxis() == wxMOUSE_WHEEL_VERTICAL)
    {
      m_currentEffect->changeYParameter(step);
    }
    else if (event.GetWheelAxis() == wxMOUSE_WHEEL_HORIZONTAL)
    {
      m_currentEffect->changeXParameter(step);
    }
    wxClientDC dc(this);
    render(dc);
  }
}

void ImagePanel::render(wxDC& dc)
{
  if (m_currentEffect != nullptr)
  {
    wxImage imageAfterEffect = m_image.Copy();
    m_currentEffect->apply(imageAfterEffect);
    dc.DrawBitmap(imageAfterEffect, 0, 0, false);
  }
  else
  {
    dc.DrawBitmap(m_image, 0, 0, false);
  }
}
