#pragma once
#include <memory>

#include "Effect.h"

class KernelEffect : public Effect
{
 public:
  KernelEffect(std::string name, std::string description, int kernelSize);
  virtual ~KernelEffect();
  virtual void apply(wxImage& image) const override;
  virtual void changeXParameter(int deltaX) override;
  virtual void changeYParameter(int deltaY) override;
  virtual void setKernelSize(int kernelSize);

 protected:
  virtual void generateKernel() = 0;
  std::shared_ptr<double[]> m_kernel;
  int m_kernelSize;
  int m_kernelMinSize;
  int m_kernelMaxSize;
  int m_kernelStep;  // kernelStep is 2 by default - odd / even kernel usually should remain odd / even
};
