#pragma once
#include "KernelEffect.h"

class BlurEffect : public KernelEffect
{
 public:
  BlurEffect(int kernelSize);
  virtual ~BlurEffect();
  void generateKernel() override;
};
