#pragma once
#include <wx/image.h>

#include <string>
class Effect
{
 public:
  Effect(std::string name, std::string description);
  virtual ~Effect();
  virtual void apply(wxImage& image) const = 0;
  virtual void changeXParameter(int deltaX) = 0;
  virtual void changeYParameter(int deltaY) = 0;
  const std::string& getName() const;
  const std::string& getDescription() const;

 protected:
  static unsigned char roundPixelValue(double value);

 protected:
  std::string m_name;
  std::string m_description;
};
