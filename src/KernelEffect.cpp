#include "KernelEffect.h"

#include "wx/rawbmp.h"

KernelEffect::KernelEffect(std::string name, std::string description, int kernelSize)
    : Effect(name, description), m_kernelSize(kernelSize), m_kernelMinSize(1), m_kernelMaxSize(9), m_kernelStep(2)
{
}

KernelEffect::~KernelEffect() {}

void KernelEffect::apply(wxImage& image) const
{
  wxImagePixelData::Iterator pixelIterator(image);
  int padding = m_kernelSize / 2;
  pixelIterator.Reset(image);
  pixelIterator.Offset(image, padding, padding);
  for (int row = padding; row < image.GetHeight() - padding; row++)
  {
    wxImagePixelData::Iterator rowStart = pixelIterator;
    for (int column = padding; column < image.GetWidth() - padding; column++)
    {
      // apply kernel to the neighborhood of (column, row)
      double pixelValue[3] = {0};
      wxImagePixelData::Iterator kernelIterator = pixelIterator;
      kernelIterator.Offset(image, -padding, -padding);
      for (int k = 0; k < m_kernelSize; k++)
      {
        wxImagePixelData::Iterator kernelRowStart = kernelIterator;
        for (int l = 0; l < m_kernelSize; l++)
        {
          int index = k * m_kernelSize + l;
          pixelValue[0] += kernelIterator.Red() * m_kernel[index];
          pixelValue[1] += kernelIterator.Green() * m_kernel[index];
          pixelValue[2] += kernelIterator.Blue() * m_kernel[index];
          kernelIterator++;
        }
        kernelIterator = kernelRowStart;
        kernelIterator.OffsetY(image, 1);
      }
      pixelIterator.Red() = roundPixelValue(pixelValue[0]);
      pixelIterator.Green() = roundPixelValue(pixelValue[1]);
      pixelIterator.Blue() = roundPixelValue(pixelValue[2]);
      pixelIterator++;
    }
    pixelIterator = rowStart;
    pixelIterator.OffsetY(image, 1);
  }
}

void KernelEffect::changeYParameter(int deltaX)
{
  // by default - all kernel effects y operation changes the kernel size
  setKernelSize(m_kernelSize + deltaX * m_kernelStep);
}

void KernelEffect::changeXParameter(int deltaY) {}

void KernelEffect::setKernelSize(int kernelSize)
{
  if (kernelSize > m_kernelMinSize && kernelSize < m_kernelMaxSize)
  {
    m_kernelSize = kernelSize;
    generateKernel();
  }
}
