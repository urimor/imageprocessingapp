#include "BlurEffect.h"

#include <wx/rawbmp.h>

#include <memory>
using namespace std;

BlurEffect::BlurEffect(int kernelSize) : KernelEffect("Blur", "Make the image blurred", kernelSize)
{
  generateKernel();
  m_kernelMaxSize = 20;
}

BlurEffect::~BlurEffect() {}

void BlurEffect::generateKernel()
{
  int kernelSizeSquared = m_kernelSize * m_kernelSize;
  double value = 1.0 / kernelSizeSquared;
  m_kernel = shared_ptr<double[]>(new double[kernelSizeSquared]);
  for (size_t i = 0; i < kernelSizeSquared; i++)
  {
    m_kernel[i] = value;
  }
}
