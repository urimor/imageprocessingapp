#include "ImageProcessingApp.h"

#include <wx/sizer.h>

#include <memory>

#include "BlurEffect.h"
#include "KernelEffect.h"
#include "MainFrame.h"
#include "QuantizeEffect.h"
using namespace std;

wxIMPLEMENT_APP(ImageProcessingApp);

bool ImageProcessingApp::OnInit()
{
  wxInitAllImageHandlers();

  MainFrame *frame = new MainFrame();
  frame->AddEffect(nullptr, "Reset Image", "Remove all effects and display the original image");
  frame->AddEffect(std::make_shared<BlurEffect>(5));
  frame->AddEffect(std::make_shared<QuantizeEffect>(10));

  frame->Show(true);
  return true;
}
