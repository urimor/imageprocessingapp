#pragma once
#include "Effect.h"

class QuantizeEffect : public Effect
{
 public:
  QuantizeEffect(unsigned char numberOfColors);
  virtual ~QuantizeEffect();
  virtual void apply(wxImage& image) const;
  virtual void changeXParameter(int deltaX);
  virtual void changeYParameter(int deltaY);

 private:
  void quantizePixelValue(unsigned char& value) const;
  unsigned char m_numberOfcolors;
};
