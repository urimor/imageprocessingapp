# Image processing app

## usage
currently only png files are supported.

To open a file go to File -> Open

To apply an effect, select an effect from the effects menu. The effects parameters can be controlled by the arrow keys or ouse wheel. Each effect may have 2 parameters:
-  Y axis parameter (controlled by mouse wheel up/down or arrow kry up/down). 
-  X axis parameter (controlled by mouse wheel up/down or arrow kry up/down). 

To save the image after applying the effects go to File -> Save

## Software structure

### ImageProcessingApp
a wrapper class for the ui. The application is based on wxWidgets.
### MainFrame 
The main UI frame controlling the menus and status bar
### ImagePanel
A UI componenet that is responsible to display the image. it manages the applying of effects
### Effect
The effect class was designed  so that it will be easy to add more effects as plugins.

Each effect has a name and descriprion, and 3 main methods (pure abstract in the base class)

- ```apply(wxImage& image)```:  
  the core method that applies the effect on the image
- ```changeXParameter(int deltaX)``` / ```changeYParameter(int deltaX)```:  
  change the effect parameters

To add an effect, create a new class that is derived from ```Effect```.  
Add the effect to the main frame. This will sutomatically add the effect to the effects menu and binf the button to apply the effect

#### KernelEffect
This is a base class of all the Effects that can be represented by a simple kernel for convulution with the image 
  

## Compilation instructions
The only dependency is wxWidgets